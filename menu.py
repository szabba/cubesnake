# -*- coding: utf-8 -*-


__all__ = ['Menu']


import pyglet
from pyglet.window import key


class Menu(object):
    """The main menu"""

    def __init__(self, newgame, highscore, help, quit):
        """Menu(newgame, highscore, help, quit) -> a main menu"""

        self.__pointer_moved = True
        self.__pointer_position = 0
        self.__levels = [newgame, highscore, help, quit]
        self.__sprites = {}

        self.__sprites['Logo'] = pyglet.sprite.Sprite(
            pyglet.image.load('logo.png'))
        self.__sprites['Pointer'] = pyglet.sprite.Sprite(
            pyglet.image.load('pointer.png'))
        self.__sprites['New Game'] = pyglet.sprite.Sprite(
            pyglet.image.load('new_game.png'))
        self.__sprites['Highscores'] = pyglet.sprite.Sprite(
            pyglet.image.load('highscores.png'))
        self.__sprites['Help'] = pyglet.sprite.Sprite(
            pyglet.image.load('help.png'))
        self.__sprites['Quit'] = pyglet.sprite.Sprite(
            pyglet.image.load('quit.png'))

        self.__button_names = ['New Game', 'Highscores', 'Help', 'Quit']

        self.__separator = 200
        self.__margin = 200 - self.__sprites[self.__button_names[0]].width


    def update(self, keys, level_box):
        """M.update(keys, level_box)

        Reacts to the player using the keyboard.
        """
        
        if keys.is_just_down(key.LEFT) and self.__pointer_position > 0:

            self.__pointer_position = self.__pointer_position - 1
            self.__pointer_moved = True

        elif keys.is_just_down(key.RIGHT) and self.__pointer_position < 3:

            self.__pointer_position = self.__pointer_position + 1
            self.__pointer_moved = True


        if keys.is_just_down(key.ENTER):

            level_box.change_level(self.__levels[self.__pointer_position])


        if self.__pointer_moved:
 
            x = self.__margin / 2
            y = 30 + (200 - self.__sprites[self.__button_names[0]].height) / 2

            for image in self.__button_names:

                self.__sprites[image].x = x
                self.__sprites[image].y = y

                x = x + self.__separator
	    
            self.__sprites['Logo'].x = (800 - self.__sprites['Logo'].width) / 2
            self.__sprites['Logo'].y = 230 + (600 - 230 - self.__sprites['Logo'].height) / 2

            delta_x = self.__pointer_position * self.__separator
            self.__sprites['Pointer'].x = 100 + delta_x - self.__sprites['Pointer'].width / 2 
            self.__sprites['Pointer'].y = (30 - self.__sprites['Pointer'].height) / 2

            self.__sth_changed = False


    def render(self, win_size):
        """M.render((width, height))

        Renders a window.
        """

        width, height = win_size

        self.__sprites['Logo'].draw()
        self.__sprites['Pointer'].draw()
        self.__sprites['New Game'].draw()
        self.__sprites['Highscores'].draw()
        self.__sprites['Help'].draw()
        self.__sprites['Quit'].draw()
