# -*- coding: utf-8 -*-


__all__ = ['LevelBox', 'KeysState', 'JUST_DOWN', 'DOWN', 'JUST_UP', 'UP']


import pyglet


JUST_DOWN, DOWN, JUST_UP, UP = range(4)


class KeysState(object):
    """State of the keyboard.

    Use the `is_` family of methods to query the state of the keyboard.
    """

    def __init__(self):
        """KeysState() -> a KeysState"""

        self.__keys_state = {}

    def down(self, symbol):
        """KS.down(symbol)

        Report that a key has been pressed.
        """

        self.__keys_state[symbol] = JUST_DOWN

    def up(self, symbol):
        """KS.up(symbol)

        Report that a key has been unpressed.
        """

        self.__keys_state[symbol] = JUST_UP

    def update(self):
        """KS.update()

        Turns all JUST_* states of keys to
        corresponding non-JUST_* states.
        """

        for symbol, state in self.__keys_state.items():

            if state is JUST_DOWN:

                self.__keys_state[symbol] = DOWN

            if state is JUST_UP:

                self.__keys_state[symbol] = UP

    def state_one_of(self, symbol, *states):
        """KS.__state_one_of(symbol, *states) -> bool

        Check whether the key with the given symbol is in one of the
        given states.
        """

        if symbol not in self.__keys_state:

            return False

        return self.__keys_state[symbol] in states

    def is_down(self, symbol):
        """KS.is_down(symbol) -> bool"""

        return self.state_one_of(symbol, DOWN, JUST_DOWN)

    def is_just_down(self, symbol):
        """KS.is_just_down(symbol) -> bool"""

        return self.state_one_of(symbol, JUST_DOWN)

    def is_up(self, symbol):
        """KS.is_up(symbol) -> bool"""

        return self.state_one_of(symbol, UP, JUST_UP)

    def is_just_up(self, symbol):
        """KS.is_just_up(symbol) -> bool"""

        return self.state_one_of(symbol, JUST_UP)


class LevelBox(object):
    """A container for levels"""

    def __init__(self, window, init_level, fps=32):
        """LevelBox(window, init_level) -> a container for levels"""

        self.__window = window
        self.__level = init_level
        self.__keys_state = KeysState()

        self.__fps_display = pyglet.clock.ClockDisplay()

        pyglet.clock.schedule_interval(self.update, 1.0 / fps)

    def change_level(self, new_level):
        """LB.change_level(new_lebel)

        Changes the level in the level box.
        """

        self.__level = new_level

    def on_resize(self, width, height):
        """LB.one_resize(width, height)

        Event handler for window resizes.
        """

        self.__size = (width, height)

    def on_key_press(self, symbol, modifiers):
        """LB.on_key_press(symbol, modifiers)

        Event handler for key presses.
        """

        self.__keys_state.down(symbol)

        # TODO: Uncomment when stable; for now we want to keep the
        # default behaviour of closing the window with a press to Escape
        #
        # return True

    def on_key_release(self, symbol, modifiers):
        """LB.on_key_release(symbol, modifiers)

        Event handler for key releases"""

        self.__keys_state.up(symbol)

    def update(self, dt):
        """LB.update(dt)

        Forwards time.
        """

        if self.__level is None:

            pyglet.app.exit()

        else:

            self.__level.update(self.__keys_state, self)
            self.__keys_state.update()

    def on_draw(self):
        """LB.on_draw()

        Renders a level.
        """
        self.__window.clear()

        if self.__level is not None:

            self.__level.render(self.__size)

        self.__fps_display.draw()
