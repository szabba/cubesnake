# -*- coding: utf-8 -*-


import pyglet

import level_box
import menu


if __name__ == '__main__':

    game_win = pyglet.window.Window(
            width=800,
            height=600)

    game_win.push_handlers(level_box.LevelBox(
        game_win,
        menu.Menu(None, None, None, None)))

    pyglet.app.run()
