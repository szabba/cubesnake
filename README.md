# Cube Snake

Snake. On the surface of a cube. Nothing fancy.

# Running it

Don't expect much yet.

```bash
$ python2 main.py
```

It should show you a menu with four options. Navigate between them with
left and right arrow keys. Choose one with Enter. Choosing any of them
will close the game.

# License

Copyright (C) 2013 Karol Marcjan <karol.marcjan@gmail.com>
                   Bartosz Boguniewicz <b.boguniewicz@gmail.com>
                   Wiktor Wolak <ruwi@o2.pl>

Licensed under the WTFPL
